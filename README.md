# PAV's Remote Working Guideline

## Introduce
- In response to the uncertainties presented by Covid-19, many companies have asked their employees to work remotely.

- In times of crisis or other rapidly changing circumstances, the current level of preparation may not be feasible. Therefore, PAV should also have a *Remote Working Guideline*.

## Rule of Remote Work

Even if PAV support flexibility, some rules should be applied as this helps employees feel like they are a part of your organizational culture. 

[Rule of Remote Work](./rule/README.md) 

## How Managers Can Support Remote Employees
These are just a few of the most common challenges with managing remote teams. 
Understanding these unique challenges and working through them will improve engagement, productivity, and cohesion across your entire team.

[Tips for Managing Remote Employees](./manager/README.md) 

## How Do I Measure the Success of a Remote Work Rule?
According to research, 39% of people working from home complete their tasks faster than those in fixed workplaces.

However, in order to set them up for success, employers have to set clear and measurable goals for their remote employees. 

When performing a review, manager considers three points of view:
- His own opinions.
- Thoughts from co-workers.
- A self-evaluation from the actual employee.