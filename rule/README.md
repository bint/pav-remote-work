# 1. Rule brief & purpose
We designed our work from home rule to make sure that working from home is beneficial to our employees and company.

# 2. Scope

PAV's work from home rule applies to *all our employees* who prefer working from home. 

# 3. Rule elements

Employees work from home or telecommute:
- Full-time
- On certain days
- Everyday, dividing the schedule between being present at the office and working from a remote location.

# 4. Rule Reasons

Work from home arrangements can be occasional, temporary or permanent.

Reasons that could demand telecommuting include but are not limited to:

- Parenting
- Bad weather
- Emergencies
- Medical reasons
- Work-life balance
- Overlong commute

Other reasons for working from home depend on CEO’ judgement.

# 5. How to determine whether an employee can work from home

PAV's managers to consider these elements before approving work from home:

1. Is PAV's employee eligible by nature of their job?
2. Are there any security and data privacy concerns?
3. Will collaboration with the employee’s team become difficult?
4. Do employees have the necessary equipment or software installed at home? (such as VPN, ..)
5. What are the conditions of employees’ home or alternative place of work (noise, internet connection etc.)

# 6. Requesting Work from Home Procedure
When PAV's employees plan to work from home, this procedure must be followed:

1. Employees send a request through email to his managers at least *[two days]* in advance.
2. Their managers must approve their request considering all elements at point 5.
3. If the work from home arrangement spans for more than a week, managers and team members should meet to discuss details and set specific goals, schedules and deadlines.

PAV's employees who need to work from home for unforeseen reasons (e.g. illness or temporary difficult commute) should file their request as soon as possible, so managers can consider and approve it.

# 7. Compensation and benefits

Usually, work from home arrangements don’t affect employees’ employment terms. 

If working from home has any effect on compensation and benefits, then HR is responsible to create a *new contract*. 