# How Managers Can Support Remote Employees
Actions that manager can take today include:

## 1. Have a Daily Check-In
Whenever possible, this should be one-on-one, and face-to-face via video. 
Your member needs to see you, and you need to see them. 

Frequent check-ins with remote workers is a great way to help them overcome challenges that come with remote work. 

Waiting for an employee to speak up may be too late. In fact, many of them will never decide to raise a problem themselves. 

## 2. Communicate a Lot
It probably goes without saying that you should be in regular communication with your team. 
One of the hardest things about working from home, especially if you're used to an office environment, is the sense of loneliness and isolation that can set in. 

## 3. Manage Expectations
Help your team figure out what they should do, and create realistic expectations for their work. By the way, "managing expectations" applies to you as a manager as well. 

That means defining the scope, deadlines, and deliverables for each task or project your team is working on. 

## 4. Resource Your Team
Make sure your team has the technology it needs to get the work done. 
They need tools like laptops, high-speed internet connection, software such as VPN ...
